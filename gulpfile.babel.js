'use strict';
import gulp from 'gulp';
import util from 'gulp-util';
import cssnano from 'cssnano';
import pcss from 'gulp-postcss';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import nested from 'postcss-nested';
import cssnext from 'postcss-cssnext';
import browserSync from 'browser-sync';
import pcssimport from 'postcss-import';
import pcssmixins from 'postcss-mixins';
import sourcemaps from 'gulp-sourcemaps';
import pcssurl from 'postcss-url';

const devPCSS = './src/css';
const devJS = './src/js/**.js';
const dist = './app/';

const processors = [
  pcssimport(),
  pcssurl(),
  pcssmixins({ mixinsDir: './src/css/mixins' }),
  cssnext({ browsers: ['last 3 version'] }),
  nested(),
  cssnano({ autoprefixer: false })
];

gulp.task('serve', ['build'], function () {
  browserSync.init({
    server: {
      baseDir: dist
    }
  });
  gulp.watch(devPCSS + '/**/**/**/**.pcss', ['pcss']);
  gulp.watch(devJS, ['js']);
  gulp.watch('./app/**/**/**/**.html', browserSync.reload);
});

gulp.task('watch', ['build'], function () {
  gulp.watch(devPCSS + '/**/**/**/**.pcss', ['pcss-build']);
  gulp.watch(devJS, ['js-build']);
});

gulp.task('pcss', function () {
  return (
    gulp.src(devPCSS + '/*.pcss')
      .pipe(
      rename({
        extname: ".css"
      })
      )
      .pipe(
      sourcemaps.init()
      )
      .pipe(
      pcss(processors)
      )
      .pipe(
      sourcemaps.write('./css-source-maps/')
      )
      .pipe(
      gulp.dest(dist + '/css/')
      )
      .pipe(
      browserSync.stream()
      )
  )
});

gulp.task('pcss-build', function () {
  return (
    gulp.src(devPCSS + '/*.pcss')
      .pipe(
      rename({
        extname: ".css"
      })
      )
      .pipe(
      sourcemaps.init()
      )
      .pipe(
      pcss(processors)
      )
      .pipe(
      sourcemaps.write('./css-source-maps/')
      )
      .pipe(
      gulp.dest(dist + '/css/')
      )
  )
});
gulp.task('js', function () {
  return (
    gulp.src(devJS)
      .pipe(
      uglify()
      )
      .pipe(
      gulp.dest(dist + '/js/')
      )
      .pipe(
      browserSync.stream()
      )
  )
});
gulp.task('js-build', function () {
  return (
    gulp.src(devJS)
      .pipe(
      uglify()
      )
      .pipe(
      gulp.dest(dist + '/js/')
      )
  )
});
gulp.task('build', ['pcss-build', 'js-build']);
gulp.task('default', ['serve']);
