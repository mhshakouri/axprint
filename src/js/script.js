// Using HandleBars JS to ease the pain of repetive data inputs manually with a json data.js file
function showTemplate(template, data, position) {
  $(position).html(template(data));
}
function sourcomp(identifier, data) {
  return Handlebars.compile($(identifier).html());
}
$(document).ready(function () {
  weather_temp = sourcomp("#weather_temp");
  livecams_temp = sourcomp("#livecams_temp");
  showTemplate(weather_temp, weather_data, "#weather_content");
  showTemplate(livecams_temp, livecams_data, "#livecams_content");
  $('.weather-box .card').click(function () {
    if ($(this).hasClass('active')) {
      return null;
    } else {
      $('.weather-box .card.active').toggleClass('active');
      $(this).toggleClass('active');
    }
  });
  $('.livecam-box .card').click(function (event) {
    event.preventDefault();
    $('.modal-player').toggleClass('open');
  });
  $('a.modal-close').click(function (event) {
    event.preventDefault();
    $('.modal-player').toggleClass('open');
  })
});