// Using HandleBars JS to ease the pain of repetive data inputs manually with a json data.js file
var weather_data = {
	days: [
		{
			day: "Monday",
			date: "6 Oct",
			city: "New York",
			temp: "23° C",
			night_temp: "18°",
			icon_class: "sunny-day",
			humidity: "20",
			wind_speed: "18",
			wind_direction: "east"
		},
		{
			day: "Tuesday",
			date: "7 Oct",
			city: "New York",
			temp: "23° C",
			night_temp: "18°",
			icon_class: "partly-cloudy",
			humidity: "20",
			wind_speed: "18",
			wind_direction: "east"
		},
		{
			day: "Wednesday",
			date: "8 Oct",
			city: "New York",
			temp: "23° C",
			night_temp: "18°",
			icon_class: "cloudy",
			humidity: "20",
			wind_speed: "18",
			wind_direction: "east"
		},
		{
			day: "Thursday",
			date: "9 Oct",
			city: "New York",
			temp: "23° C",
			night_temp: "18°",
			icon_class: "cloudier",
			humidity: "20",
			wind_speed: "18",
			wind_direction: "east"
		},
		{
			day: "Friday",
			date: "10 Oct",
			city: "New York",
			temp: "23° C",
			night_temp: "18°",
			icon_class: "cloudy-thunder",
			humidity: "20",
			wind_speed: "18",
			wind_direction: "east"
		},
		{
			day: "Saturday",
			date: "11 Oct",
			city: "New York",
			temp: "23° C",
			night_temp: "18°",
			icon_class: "rainy",
			humidity: "20",
			wind_speed: "18",
			wind_direction: "east"
		},
		{
			day: "Sunday",
			date: "12 Oct",
			city: "New York",
			temp: "23° C",
			night_temp: "18°",
			icon_class: "rainier",
			humidity: "20",
			wind_speed: "18",
			wind_direction: "east"
		},
	]
};

var livecams_data = {
	cams: [
		{
			city: "New York",
			date: "8 Oct",
			time: "8:00 AM",
			image: "ny.png"
		},
		{
			city: "Los Angeles",
			date: "8 Oct",
			time: "8:00 AM",
			image: "la.png"
		},
		{
			city: "Chicago",
			date: "8 Oct",
			time: "8:00 AM",
			image: "ch.png"
		},
		{
			city: "London",
			date: "8 Oct",
			time: "8:00 AM",
			image: "london.png"
		}
	]
};
